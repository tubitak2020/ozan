#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A module for background processes
"""

import testi
import saz


def foursEdit(data, fours):
    dataList = []
    pre = 0
    for move in fours:
        flagData = []
        for note in range(move-pre):
            flagData.append(data[note+pre])
        dataList.append(flagData)
        pre = fours[fours.index(move)]
    return dataList


def addZero(datas, zero):
    dataList = datas
    for data in dataList:
        diff = zero-len(data)
        data += ["0"]*diff
    return dataList


def typeInt(datas):
    dataList = datas
    for data in dataList:
        for number in data:
            number = int(number)
    return dataList


def getFours(notes, values):
    fours = []
    plus = 0
    for value in range(len(values)):
        plus += int(values[value])
        if plus >= 32:
            fours.append(value+1)
            plus = 0
    noteList = typeInt(addZero(foursEdit(notes, fours), 32))
    valueList = typeInt(addZero(foursEdit(values, fours), 32))
    return [noteList, valueList]


def makeInt(datas):
    notes = []
    values = []
    for data in datas[0]:
        note = []
        for move in data:
            note.append(int(move))
        notes.append(note)
    for data in datas[1]:
        value = []
        for move in data:
            value.append(int(move))
        values.append(value)
    return [notes, values]


def getData(file, fileType):
    data = saz.convert(file, fileType)
    notes = [note[0] for note in data]
    values = [value[1] for value in data]
    stringList = getFours(notes, values)
    return makeInt(stringList)


def getNoteNetwork():
    nodes = [64, 70, 80, 10]
    thres = [8, 8, "sin"]
    return testi.NeuralNet(nodes, thres)


def getValueNetwork():
    nodes = [64, 70, 80, 10]
    thres = [0.5, 0.5, "sin"]
    return testi.NeuralNet(nodes, thres)


def train(network, datas, turn, alpha):
    for move in range(turn):
        dataList = datas + [[0]*64]
        for data in range(len(dataList)-1):
            network.train(dataList[data], dataList[data+1][:-22], alpha)


def merge(datas):
    dataList = datas
    writeData = []
    for data in dataList:
        for turn in range(10):
            writeData.append([data[0][turn], data[1][turn]])
    return writeData


def translateNote(note):
    if note >= 15.5:
        return 16
    elif note >= 14.5:
        return 15
    elif note >= 13.5:
        return 14
    elif note >= 12.5:
        return 13
    elif note >= 11.5:
        return 12
    elif note >= 10.5:
        return 11
    elif note >= 9.5:
        return 10
    elif note >= 8.5:
        return 9
    elif note >= 7.5:
        return 8
    elif note >= 6.5:
        return 7
    elif note >= 5.5:
        return 6
    elif note >= 4.5:
        return 5
    elif note >= 3.5:
        return 4
    elif note >= 2.5:
        return 3
    elif note >= 1.5:
        return 2
    elif note >= 0.5:
        return 1
    else:
        return 0


def translateValue(value):
    if value >= .99:
        return 32
    elif value >= .96:
        return 16
    elif value >= .69:
        return 8
    elif value >= .28:
        return 4
    elif value >= .005:
        return 2
    else:
        return 1


def translate(notes, values):
    noteList = []
    valueList = []
    for note in notes:
        noteList.append(translateNote(note))
    for value in values:
        valueList.append(translateValue(value))
    return [noteList, valueList]


def write(noteNet, valueNet, turn, name):
    datas = []
    preNote = [0,7,6,6,7,6,6,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    preValue = [4,2,2,2,2,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for move in range(turn):
        pren = preNote + preValue
        prev = preValue + preNote
        data = translate(noteNet.result(pren), valueNet.result(prev))
        datas.append(data)
        preNote, preValue = data[0], data[1]
    writeData = merge(datas)
    stringData = ""
    for data in writeData:
        stringData += f"{data[0]} {data[1]}\n"
    stringData = stringData[:-1]
    with open(f"{name}.txt", "w+") as f:
        f.write(stringData)


def makeSong(fileName):
    saz.turn = int(fileName.split()[1])
    saz.makeSoundV(fileName)

