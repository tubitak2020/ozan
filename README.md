# Ozan

Ozan is an application that creates "bağlama" songs with machine learning
(bağlama is a traditional Turkish instrument).

Ozan uses [testi](https://gitlab.com/tubitak2020/testi) and [saz](https://gitlab.com/tubitak2020/saz) modules.
