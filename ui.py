#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A module for menage user interface
"""

from language import texts

from process import *

from os import walk

from tkinter import *
from tkinter import simpledialog
from tkinter import messagebox
from tkinter import ttk

# varibles set
lang = "tr"
neuralnets = ()
nn = {}
currentNN = ""
dataFolder = ""
turn = 0
song = ""

# window set
window = Tk()
window.title("Ozan")
window.geometry("380x300")


def createNN():
    name = simpledialog.askstring(texts[lang]["msg"]["crt"],
                                  texts[lang]["msg"]["name"])
    global neuralnets
    neuralnets += (name,)
    nn[name] = [getNoteNetwork(), getValueNetwork()]
    NN_BOX["values"] = neuralnets
    messagebox.showinfo(texts[lang]["msg"]["scs"], texts[lang]["msg"]["nn"])

# neural net set
NN_LABEL = Label(window, text=texts[lang]["nn"],
                 font=("Arial", 28), width=12)
NN_LABEL.grid(column=0, row=0)

NN_BOX = ttk.Combobox(window, width=25)
NN_BOX["values"] = neuralnets
NN_BOX.grid(column=0, row=2)

NN_BUTTON = Button(window, text=texts[lang]["create"],
                   font=("Arial", 16), width=5, command=createNN)
NN_BUTTON.grid(column=1, row=2)


def setData():
    data = DATA_ENTRY.get()
    if data[-1] != "/" or data[0] == "/" or data == "":
        messagebox.showwarning(texts[lang]["msg"]["war"],
                               texts[lang]["msg"]["wdata"])
    else:
        messagebox.showinfo(texts[lang]["msg"]["scs"],
                            texts[lang]["msg"]["data"])

# data set
DATA_LABEL = Label(window, text=texts[lang]["data"],
                   font=("Arial", 28), width=12)
DATA_LABEL.grid(column=0, row=4)

DATA_ENTRY = Entry(window,
                   font=("Arial", 12), width=24)
DATA_ENTRY.grid(row=6)

DATA_BUTTON = Button(window, text=texts[lang]["set"],
                     font=("Arial", 16), width=5, command=setData)
DATA_BUTTON.grid(column=1, row=6)


def createSong(name):
    currentNN = NN_BOX.get()
    sname = name
    write(nn[currentNN][0], nn[currentNN][1], 100, sname)
    makeSong(sname)
    # messagebox.showinfo('Succes!', 'Creating succesfully finished!')


def trainNeuralNet():
    currentNN = NN_BOX.get()
    dataFolder = DATA_ENTRY.get()
    turn = int(TURN_SPIN.get())
    quar = int(turn/5)
    files = []
    for (dirpath, dirnames, filenames) in walk(dataFolder):
        files.extend(filenames)
        break
    datas = []
    for file in files:
        datas.append(file[:-4])
    print("***")
    alpha = [0.005, 0.001]
    for step in range(turn):
        if step%(int(turn/10)) == 0:
            print(f"---\n{step}\n---")
        for data in datas:
            dataList = getData(f"{dataFolder}{data}", "v")
            notes = dataList[0]
            values = dataList[1]
            train(nn[currentNN][0], notes+values, 1, alpha[0])
            train(nn[currentNN][1], values+notes, 1, alpha[1])
        if step == turn-1 or step%quar == 0:
            createSong(f"Song {step+1}")
            print(f"---\n{step+1} <--\n---")
    print("***")
    messagebox.showinfo(texts[lang]["msg"]["scs"], texts[lang]["msg"]["turn"])

# turn set
TURN_LABEL = Label(window, text=texts[lang]["turn"],
                   font=("Arial", 28), width=12)
TURN_LABEL.grid(column=0, row=8)

TURN_SPIN = Spinbox(window, from_=1, to=1000000, width=25)
TURN_SPIN.grid(column=0, row=10)

TURN_BUTTON = Button(window, text=texts[lang]["train"],
                     font=("Arial", 16), width=5, command=trainNeuralNet)
TURN_BUTTON.grid(column=1, row=10)

window.mainloop()

